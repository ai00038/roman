/**
 * We want to convert roman numerals to arabic numerals
 * 
 *  Symbol	I	V	X	L	C	D	M
 *  Value	1	5	10	50	100	500	1,000
 * 
 *  Subtractive Notation
 *  Number	    4	9	40	90	400	900
 *  Notation	IV	IX	XL	XC	CD	CM
 * 
 *  Some examples of the modern use of Roman numerals include:
 *  1954 as MCMLIV
 *  1990 as MCMXC
 *  2014 as MMXIV
 * 
 */

const parseOne = (numeral) => {
    const numeralsMap = { I: 1, V: 5, X: 10, L: 50, C: 100, D: 500, M: 1000 };
    return numeralsMap.hasOwnProperty(numeral) ? numeralsMap[numeral] : 0 ;
}

const hasSpecial = (first, second) => {
    let twoNumerals = first + second;
    const specialNumerals = ['IV', 'IX', 'XL', 'XC', 'CD', 'CM'];
    return specialNumerals.indexOf(twoNumerals) > -1;
}

const parseSpecial = (first, second) => {
    let twoNumerals = first + second;
    const numeralsMap = { IV: 4, IX: 9, XL: 40, XC: 90, CD: 400, CM: 900 };
    return numeralsMap.hasOwnProperty(twoNumerals) ? numeralsMap[twoNumerals] : 0 ;
}

const convertFromRomanToArabic = (romString) => {
    let sum = 0;
    let romArray = romString.split('');

    while (romArray.length) {
        let first = romArray[0];
        let second = (romArray.length >= 2) ?  romArray[1] : 'n/a';
        sum += hasSpecial(first, second) ? parseSpecial(first, second) : parseOne(first) ;
        hasSpecial(first, second) ? romArray.splice(0, 2) : romArray.splice(0, 1);
    }

    return sum;
}

// does not protect against invalid characters or invalid ordering of characters

console.log(convertFromRomanToArabic('XXIV'));
console.log(convertFromRomanToArabic('MCMLIV'));
console.log(convertFromRomanToArabic('MCMXC'));
console.log(convertFromRomanToArabic('MMXIV'));